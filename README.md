# 简介

本项目是SpringBoot3.2.5 集成SpringSecurity和SpringSecurity OAuth2的Demo，仅包含了登录和OAuth2授权。

# 技术选型

 - JDK: 17
 - 核心：SpringBoot 3.2.5 
 - 权限框架：SpringSecurity, SpringSecurity Oauth2
 - 模板引擎：Thymeleaf
 - 会话管理：SpringSession
 - 前台UI：Vue3 ElementUI

# 模块简介
 - auth-server: 普通授权服务器，OAuth2授权服务器
 - oauth2-client: OAuth2客户端，如果授权到期或者缺少授权自动跳转到授权服务器进行授权
 - oauth2-resource: OAuth2资源服务器，凭借OAuth2授权服务器返回的access_token进行资源访问
 - user-server: 正常业务服务，通过普通授权服务器登录接口返回的token进行授权访问，token通过header或者url表单参数传递（X-Auth-Token）
 
# 实现细节
 
 - SpringSecurity控制全部授权
 
 - SpringSecurity Oauth2提供OAuth2相关技术实现
# 让Demo跑起来

1.在本地执行 /src/test/resources/oauth2_authorization.sql

2.默认登录用户名admin/123123，默认登录手机验证码是13312341234/1111

# OAuth2接口调用实例
 - 获取Code授权（浏览器直接GET请求）：http://127.0.0.1:10050/oauth2/authorize?response_type=code&client_id=manager_web&scope=openid all&redirect_uri=https://www.baidu.com
 - 通过CODE获取AccessToken、RefreshToken: curl --location --request POST 'http://127.0.0.1:10050/oauth2/token' \
   --header 'Authorization: Basic bWFuYWdlcl93ZWI6MTIzMTIz' \
   --header 'Content-Type: application/x-www-form-urlencoded' \
   --data-urlencode 'grant_type=authorization_code' \
   --data-urlencode 'redirect_uri=https://www.baidu.com' \
   --data-urlencode 'code=授权回调回来的CODE'
 - 刷新AccessToken: curl --location --request POST 'http://127.0.0.1:10050/oauth2/token' \
   --header 'Authorization: Basic bWFuYWdlcl93ZWI6MTIzMTIz' \
   --header 'Content-Type: application/x-www-form-urlencoded' \
   --data-urlencode 'grant_type=refresh_token' \
   --data-urlencode 'refresh_token=刷新的RefreshToken'
 - 撤销AccessToken: curl --location --request POST 'http://127.0.0.1:10050/oauth2/revoke' \
   --header 'Authorization: Basic bWFuYWdlcl93ZWI6MTIzMTIz' \
   --header 'Content-Type: application/x-www-form-urlencoded' \
   --data-urlencode 'token=AccessToken值'
 - 退出AccessToken: curl --location --request POST 'http://127.0.0.1:10050/connect/logout' \
   --header 'Authorization: Bearer AccessToken的值' \
   --header 'Referer: http://127.0.0.1:10050/connect/logout'
 - 通过AccessToken获取用户信息: curl --location --request POST 'http://127.0.0.1:10050/userinfo' \
   --header 'User-Agent: Apifox/1.0.0 (https://apifox.com)' \
   --header 'Authorization: Bearer AccessToken的值' \
 - 通过用户名密码获取AccessToken: curl --location --request POST 'http://127.0.0.1:10050/oauth2/token' \
   --header 'Authorization: Basic bWFuYWdlcl93ZWI6MTIzMTIz' \
   --header 'Content-Type: application/x-www-form-urlencoded' \
   --data-urlencode 'grant_type=password' \
   --data-urlencode 'username=admin' \
   --data-urlencode 'password=123123' \
   --data-urlencode 'scope=all openid'

# 其他地址：
 - 登录页面: [GET] http://127.0.0.1:10050/login
 - 登录接口: [POST] http://127.0.0.1:10050/login，入参username、password
 - 退出接口: [GET/POST] http://127.0.0.1:10050/logout


# 写在最后

1. 项目会话管理使用的SpringSession Redis，登录成功sessionId保存在Cookie中。
2. 如果是前后端分离项目，可自行扩展HttpSessionIdResolver接口，实现sessionId保存在Header中即可（后端获取会话信息直接通过HttpSession获取）。
3. 本项目最好使用Cookie管理会话，如果涉及到多模块项目，建议仅将本项目使用Cookie，其他项目使用Header。
4. 'Authorization: Basic bWFuYWdlcl93ZWI6MTIzMTIz'这种短的Authorization格式：Basic Base64(clientId:clientSecret)