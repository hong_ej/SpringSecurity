package cn.antcore.controller;

import jakarta.annotation.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

@RequestMapping
@RestController
public class FooController {

    @Resource
    private WebClient webClient;

    @GetMapping
    public String index(OAuth2AuthenticationToken authentication) {
        return "Hello " + authentication.getPrincipal().getName();
    }

    @GetMapping("/get")
    @PreAuthorize("hasScope('all')")
    public String get() {
        return "使用 all权限调用的该接口";
    }

    @GetMapping("/bar")
    public String bar() {
        // 调用资源服务器接口
        return webClient.get().uri("http://127.0.0.1:10040/bar").retrieve().bodyToMono(String.class).block();
    }
}
