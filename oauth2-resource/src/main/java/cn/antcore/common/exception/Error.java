package cn.antcore.common.exception;

import lombok.Getter;

import java.io.Serializable;

@Getter
public abstract class Error implements Serializable {
    private final String code; //错误码
    private final String message;   //错误内容

    public Error(Error error) {
        this.code = error.getCode();
        this.message = error.getMessage();
    }

    public Error(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String toString() {
        return this.message + "(" + this.code + ")";
    }

}
