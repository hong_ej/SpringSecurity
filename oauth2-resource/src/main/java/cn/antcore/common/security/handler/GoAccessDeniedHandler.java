package cn.antcore.common.security.handler;

import cn.antcore.common.exception.ErrorCode;
import cn.antcore.common.exception.ReturnT;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import java.io.IOException;

/**
 * 没有访问权限处理类
 * <br>
 * Created by Hong 2024/5/30
**/
public class GoAccessDeniedHandler extends AbstractAuthenticationHandler implements AccessDeniedHandler {

    public GoAccessDeniedHandler(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        failure(response, ReturnT.fail(ErrorCode.PAGES_401_ERROR));
    }
}
