package cn.antcore.common.security.handler;

import cn.antcore.common.exception.ErrorCode;
import cn.antcore.common.exception.ReturnT;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import java.io.IOException;

/**
 * 未登录 或者 token 失效 处理类
 * <br>
 * Created by Hong 2024/5/30
**/
public class GoAuthenticationEntryPoint extends AbstractAuthenticationHandler implements AuthenticationEntryPoint {

    public GoAuthenticationEntryPoint(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        failure(response, ReturnT.fail(ErrorCode.PAGES_LOGIN_ERROR));
    }
}
