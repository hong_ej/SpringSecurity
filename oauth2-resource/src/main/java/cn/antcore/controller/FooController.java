package cn.antcore.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping
@RestController
public class FooController {

    @GetMapping
    public String index() {
        return "Hello.";
    }

    @GetMapping("/bar")
    public String bar() {
        return "Hello Bar.";
    }

    @GetMapping("/get")
    @PreAuthorize("hasScope('all')")
    public String get() {
        return "使用 all权限调用的该接口";
    }

    @GetMapping("/hello")
    @PreAuthorize("hasScope('aaa')")
    public String hello() {
        return "Hello WoW";
    }
}
