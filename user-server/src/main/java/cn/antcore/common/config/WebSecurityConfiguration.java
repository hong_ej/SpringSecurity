package cn.antcore.common.config;

import cn.antcore.common.security.SessionIdResolver;
import cn.antcore.common.security.handler.GoAccessDeniedHandler;
import cn.antcore.common.security.handler.GoAuthenticationEntryPoint;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.session.web.http.HttpSessionIdResolver;

/**
 * SpringSecurity配置
 * <br>
 * Created by Hong 2024/5/28
**/
@Configuration
public class WebSecurityConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public WebSecurityCustomizer ignoringCustomizer() {
        return (web) -> web.ignoring().requestMatchers("/test");
    }

    /**
     * 从请求头中解析Session
     */
    @Bean
    public HttpSessionIdResolver httpSessionIdResolver() {
        return SessionIdResolver.xAuthToken();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http, ObjectMapper objectMapper) throws Exception {
        http.authorizeHttpRequests(auth -> auth.requestMatchers("/file/*").permitAll()
                .anyRequest().authenticated()
        );

        // 登录配置
        http.formLogin(AbstractHttpConfigurer::disable);

        // 退出登录配置
        http.logout(AbstractHttpConfigurer::disable);

        http.exceptionHandling(exception -> exception.accessDeniedHandler(new GoAccessDeniedHandler(objectMapper))
                .authenticationEntryPoint(new GoAuthenticationEntryPoint(objectMapper)));

        http.csrf(AbstractHttpConfigurer::disable);
        return http.build();
    }
}
