package cn.antcore.common.exception;

import lombok.Getter;

@Getter
public class AppException extends RuntimeException {

    private final String code;
    private final String message;

    public AppException(String code, String message) {
        super(message + "(" + code + ")");
        this.code = code;
        this.message = message;
    }

    public AppException(Error error) {
        super(error.toString());
        this.code = error.getCode();
        this.message = error.getMessage();
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "AppException{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
