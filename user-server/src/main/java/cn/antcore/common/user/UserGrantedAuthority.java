package cn.antcore.common.user;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

/**
 * 用户的权限信息
 * <br>
 * Created by Hong 2024/5/28
**/
@Data
public class UserGrantedAuthority implements GrantedAuthority {

    private String authority;

    public UserGrantedAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return "";
    }
}
