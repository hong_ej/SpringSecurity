package cn.antcore.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class LoginController {

    /**
     * 自定义的登录页面
     */
    @GetMapping("/login")
    public String index() {
        return "login";
    }
}
