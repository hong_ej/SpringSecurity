package cn.antcore.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Map;

public interface AuthService extends UserDetailsService {

    /** 根据手机号查询用户信息 **/
    UserDetails loadUserByMobile(String mobile) throws UsernameNotFoundException;

    /** 根据username查询详细信息 **/
    Map<String, Object> loadUserInfoByUsername(String username);

}
