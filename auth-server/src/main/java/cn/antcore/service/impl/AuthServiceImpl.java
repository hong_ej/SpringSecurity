package cn.antcore.service.impl;

import cn.antcore.common.user.AuthUserDetails;
import cn.antcore.service.AuthService;
import jakarta.annotation.Resource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class AuthServiceImpl implements AuthService {

    @Resource
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (!"admin".equals(username)) {
            throw new UsernameNotFoundException("手机号不存在");
        }
        // 可通过RPC或直查数据库获取用户信息
        AuthUserDetails userDetails = new AuthUserDetails("admin", passwordEncoder.encode("123123"), Collections.emptyList());
        userDetails.setMobile("13312341234");
        return userDetails;
    }

    @Override
    public UserDetails loadUserByMobile(String mobile) throws UsernameNotFoundException {
        // 手机号不存在时，可直接报错或先注册在登录
        if (!"13312341234".equals(mobile)) {
            throw new UsernameNotFoundException("手机号不存在");
        }
        // 可通过RPC或直查数据库获取用户信息
        // password穿不不加密的正确短信验证码即可
        AuthUserDetails userDetails = new AuthUserDetails("admin", "1111", Collections.emptyList());
        userDetails.setMobile("13312341234");
        return userDetails;
    }

    @Override
    public Map<String, Object> loadUserInfoByUsername(String username) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("username", username);
        claims.put("uid", 1);
        return claims;
    }
}
