package cn.antcore.common.authentication;

import org.springframework.security.oauth2.core.AuthorizationGrantType;

/**
 * 自定义GrantType
 * <br>
 * Created by Hong 2024/5/28
**/
public final class GrantType {
    
    public static final AuthorizationGrantType PASSWORD = new AuthorizationGrantType("password");
}
