package cn.antcore.common.authentication.password;

import cn.antcore.common.authentication.GrantType;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2AuthorizationGrantAuthenticationToken;

import java.util.Map;

/**
 * Password模式
 * <br>
 * Created by Hong 2024/5/28
**/
public class PasswordGrantAuthenticationToken extends OAuth2AuthorizationGrantAuthenticationToken {

    protected PasswordGrantAuthenticationToken(Authentication clientPrincipal, Map<String, Object> additionalParameters) {
        super(GrantType.PASSWORD, clientPrincipal, additionalParameters);
    }
}
