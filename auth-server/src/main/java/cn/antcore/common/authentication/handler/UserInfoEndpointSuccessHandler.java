package cn.antcore.common.authentication.handler;

import cn.antcore.common.security.handler.AbstractAuthenticationHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.server.authorization.oidc.authentication.OidcUserInfoAuthenticationToken;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import java.io.IOException;

/**
 * 处理/userinfo成功响应的结果
 * <br>
 * Created by Hong 2024/5/28
**/
public class UserInfoEndpointSuccessHandler extends AbstractAuthenticationHandler implements AuthenticationSuccessHandler {

    public UserInfoEndpointSuccessHandler(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        OidcUserInfoAuthenticationToken userInfoAuthenticationToken = (OidcUserInfoAuthenticationToken) authentication;
        success(response, userInfoAuthenticationToken.getUserInfo().getClaims());
    }
}
