package cn.antcore.common.exception;

import java.text.MessageFormat;

public final class ErrorCode extends Error {

    public static final Error SUCCESS = new ErrorCode("0000", "Success.");

    public static final Error ERROR = new ErrorCode("0001", "系统开小差了，请稍后再试");
    public static final Error PAGES_400_ERROR = new ErrorCode("0301", "无效的请求");
    public static final Error PAGES_401_ERROR = new ErrorCode("0302", "请求被限制，您没有授权");
    public static final Error PAGES_404_ERROR = new ErrorCode("0303", "访问地址不存在");
    public static final Error PAGES_405_ERROR = new ErrorCode("0304", "请求方法不被支持");
    public static final Error PAGES_500_ERROR = new ErrorCode("0305", "出现错误，请稍后再试");
    public static final Error USERNAME_ERROR = new ErrorCode("0360", "{0}");
    public static final Error ACCESS_TOKEN_ERROR = new ErrorCode("0361", "{0}");

    public ErrorCode(String code, String message) {
        super(code, message);
    }

    public ErrorCode(Error error, Object... message) {
        super(error.getCode(), MessageFormat.format(error.getMessage(), message));
    }
}
