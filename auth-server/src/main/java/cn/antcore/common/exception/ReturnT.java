package cn.antcore.common.exception;


import lombok.Getter;
import lombok.Setter;
import org.slf4j.MDC;

@Setter
@Getter
public final class ReturnT extends Error {

    private String requestId = MDC.get("requestId");

    private Object body;

    public static ReturnT success(Object content) {
        return new ReturnT(content);
    }

    public static ReturnT success() {
        return new ReturnT();
    }

    public static ReturnT fail() {
        return new ReturnT(ErrorCode.PAGES_500_ERROR);
    }

    public static ReturnT fail(Error error) {
        return new ReturnT(error);
    }

    public static ReturnT auto(Error error) {
        return new ReturnT(error);
    }

    private ReturnT() {
        super(ErrorCode.SUCCESS);
    }

    private ReturnT(Object body) {
        super(ErrorCode.SUCCESS);
        this.setBody(body);
    }

    public ReturnT(Error error) {
        super(error.getCode(), error.getMessage());
    }

    public ReturnT(String code, String message) {
        super(code, message);
    }

    public boolean isSuccess() {
        return super.getCode().equals(ErrorCode.SUCCESS.getCode());
    }
}
