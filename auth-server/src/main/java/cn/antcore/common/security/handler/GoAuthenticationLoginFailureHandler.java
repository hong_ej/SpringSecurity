package cn.antcore.common.security.handler;

import cn.antcore.common.exception.ErrorCode;
import cn.antcore.common.exception.ReturnT;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import java.io.IOException;

/**
 * 登录失败处理器
 * <br>
 * Created by Hong 2024/5/28
**/
public class GoAuthenticationLoginFailureHandler extends AbstractAuthenticationHandler implements AuthenticationFailureHandler {

    public GoAuthenticationLoginFailureHandler(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        failure(response, ReturnT.fail(new ErrorCode(ErrorCode.USERNAME_ERROR, exception.getMessage())));
    }
}
