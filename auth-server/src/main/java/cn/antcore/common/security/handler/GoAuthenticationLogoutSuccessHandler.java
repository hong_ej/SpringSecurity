package cn.antcore.common.security.handler;

import cn.antcore.common.exception.ErrorCode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import java.io.IOException;

/**
 * 退出成功处理器
 * <br>
 * Created by Hong 2024/5/28
**/
public class GoAuthenticationLogoutSuccessHandler extends AbstractAuthenticationHandler implements LogoutSuccessHandler {

    public GoAuthenticationLogoutSuccessHandler(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        success(response, ErrorCode.SUCCESS);
    }
}
