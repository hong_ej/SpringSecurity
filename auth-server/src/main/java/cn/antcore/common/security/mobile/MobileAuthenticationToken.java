package cn.antcore.common.security.mobile;

import lombok.Getter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;

/**
 * 短信验证码模式
 * <br>
 * Created by Hong 2024/5/28
**/
@Getter
public class MobileAuthenticationToken extends AbstractAuthenticationToken {

    private final Object principal;
    private String code;

    public MobileAuthenticationToken(Object principal, String code) {
        super(Collections.emptyList());
        this.principal = principal;
        this.code = code;
        setAuthenticated(false);
    }

    public MobileAuthenticationToken(Object principal, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.principal = principal;
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return this.principal;
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
    }
}
